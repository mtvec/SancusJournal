\section{Problem Statement}\label{sec:problem}
\subsection{System Model}

We consider a setting where a single infrastructure provider, $\IP$, owns and administers a (potentially large) set of microprocessor-based systems that we refer to as \emph{nodes} $N_i$.
A variety of third-party \emph{software providers} $\SP_j$ are interested in using the infrastructure provided by $\IP$.
They do so by deploying \emph{software modules} $\SM_{j,k}$ on the nodes administered by $\IP$.
\Cref{flt:system_model} provides an overview.

\begin{figure}
  \begin{center}
    \begin{tikzpicture}
        \node at (-2,2) [ip] (IP) {$IP$};
        \node [sp] (P1) at (1.5,0) {$\SP_1$};
        \node [sp] (P2) at (1.5,-2) {$\SP_2$};
        \node (D3) at (1.5,-3.5) {\Large\vdots};

        \node [sm,scale=0.7] (M11) at (-3,0) {$\SM_{1,1}$};
        \node [sm,scale=0.7] (M21) at (-2,0) {$\SM_{2,1}$};
        \node (D1) at (-1,0) {$\cdots$};
        \begin{pgfonlayer}{background}
          \node[processor, fit=(M11) (M21)(D1), inner sep=10, label=$N_1$] {};
        \end{pgfonlayer}

        \node [sm,scale=0.7] (M22) at (-3,-2) {$\SM_{2,2}$};
        \node [sm,scale=0.7] (MJK) at (-2,-2) {$\SM_{j,k}$};
        \node (D2) at (-1,-2) {$\cdots$};
        \begin{pgfonlayer}{background}
          \node[processor, fit=(M22) (MJK)(D2), inner sep=10, label=$N_2$] {};
        \end{pgfonlayer}

        \node (D3) at (-2,-3.5) {\Large\vdots};
        \draw[->,rounded corners]  (P1) to[out=120,in=70] (M11);
        \draw[->,rounded corners]  (P2) to[out=120,in=40] (M21);
        \draw[->,rounded corners]  (P2) to[out=130,in=70] (M22);
    \end{tikzpicture}
  \end{center}
  \caption{%
    Overview of our system model.
    $\IP$ provides a number of nodes $N_i$ on which software providers $\SP_j$ can deploy software modules $\SM_{j,k}$.
  }
  \label{flt:system_model}
\end{figure}

This abstract setting is an adequate model for many  ICT systems today, and the nodes in such systems can range from high-performance servers (for instance in a cloud system), over smart cards (for instance in GlobalPlatform-based systems~\cite{globalplatform}) to tiny microprocessors (for instance in sensor networks).
In this paper, we focus on the low-end of this spectrum, where nodes contain only a small embedded processor that does not support a memory management unit, protection rings, hypervisors, or other security mechanisms typically found on high-end processors.

Any system that supports extensibility (through installation of software modules) by several software providers must implement measures to make sure that the different modules cannot interfere with each other in undesired ways, either because of bugs in the software or because of malice.
For high- to mid-end systems, this problem is relatively well-understood, and good solutions exist.
Two important classes of solutions are
\begin{paraenum}
  \item the use of virtual memory, where each software module gets its own virtual address space, and where an operating system or hypervisor implements and guards communication channels between them (for instance shared memory sections or inter-process communication channels), and
  \item the use of a memory-safe virtual machine (for instance a Java VM), where software modules are deployed in memory-safe bytecode and a security architecture in the VM guards the interactions between them.
\end{paraenum}

For low-end systems with cheap microprocessors, providing adequate security measures for the setting sketched above is still an open problem, and an active area of research~\cite{s110605900}.
One straightforward solution is to transplant the higher-end solutions to these low-end systems: one can extend the processor with virtual memory, or implement a Java VM. This will be an appropriate solution in some contexts, but there are two important disadvantages.
First, the cost (in terms of required resources such as chip surface, power or performance) is non-negligible.
And second, these solutions all require the presence of a sizable trusted software layer (either the \ac{OS} or hypervisor, or the VM implementation).

The problem we address in this paper is the design, implementation and evaluation of a novel security architecture for low-end systems that is inexpensive and does not rely on any trusted software layer.
The \ac{TCB} on the networked device is \emph{only} the hardware.
More precisely, a software provider needs to trust only the hardware of the infrastructure and his own modules; he does not need to trust any infrastructural or third-party software on the nodes.
\subsection{Attacker Model}
We consider attackers with two powerful capabilities.
First, we assume attackers can manipulate \emph{all} the software on the nodes.
In particular, attackers can act as a software provider and can deploy malicious modules to nodes.
Attackers can also tamper with the operating system (for instance because they can exploit a buffer overflow vulnerability in the operating system code), or even install a completely new operating system.

Second, we assume attackers can control the communication network that is used by software providers and nodes to communicate with each other.
Attackers can sniff the network, can modify traffic, or can mount man-in-the-middle attacks.
Note that the security of the communication channel between $\IP$ and software providers is out of scope.

With respect to the cryptographic  capabilities of the attacker, we follow the Dolev-Yao attacker model~\cite{dolev-yao}: attackers cannot break cryptographic primitives, but they can perform protocol-level attacks.

Finally, attacks against the hardware of individual nodes are out of scope.
We assume the attacker does not have physical access to the hardware, cannot place  probes on the memory bus, cannot disconnect components, and so forth.
While physical attacks are important, the addition of hardware-level protections is an orthogonal problem that is an active area of research in itself~\cite{hw1,hw2,hw3,hw4}.
The addition of  hardware-level protection will be useful for many practical applications (in particular for
sensor networks) but does not have any direct impact on our proposed architecture or on the results of this paper.

Although our attacker model excludes hardware attacks, our security properties do limit the consequences of such an event (\cref{sec:problem:secprops}, \emph{hardware breach confinement}).

\subsection{Security Properties}
\label{sec:problem:secprops}

For the system and attacker model described above, we want our security architecture to enforce the following security properties:
\begin{itemize}
  \item \emph{Software module isolation.} Software modules on a node run \emph{isolated} in the sense that  no software outside the module can read or write its runtime state and code.
    The only way for other software on the node to interact with a module is by calling one of its designated entry points.
  \item \emph{Remote attestation.} A software provider can verify with high assurance that a specific software module is loaded unmodified on a specific node of $\IP$.
  \item \emph{Secure communication.} A software provider can communicate with a specific software module on a specific node with confidentiality, integrity, authenticity, and freshness guarantees.
  \item \emph{Secure linking.} A software module on a node can link with and call another module on the same node with high assurance that it is calling the intended module.
    The runtime interactions between a module $A$ and a module $B$ that $A$ links with cannot be observed or tampered with by other software on the same node.
  \item \emph{Confidential deployment.} If so desired, a software provider can deploy encrypted modules.
    This ensures that no attacker will be able to inspect the module's code at any point in time.
  \item \emph{Hardware breach confinement.} If an attacker manages to breach the hardware protections on a node, they may be able to manipulate or impersonate modules running on \emph{that} node.
    However, such a breach should not allow them to do the same with modules running on \emph{other} nodes.
\end{itemize}

Obviously, these security properties are not entirely independent of each other.
For instance, it does not make sense to have secure communication but no isolation: given the power of our attackers, any message could then  simply be modified right after its integrity was verified by a software module.
