\section{Design of Sancus}
\label{sec:design}

The main design challenge is to realize the desired security properties \emph{without trusting any software on the nodes}, and under the constraint that nodes are low-end, resource-constrained devices.
An important first design choice that follows from the resource-constrained nature of nodes is that we limit cryptographic techniques to symmetric key, in particular authenticated encryption.
While public key cryptography would simplify key management, the cost of implementing it in hardware is too high~\cite{pkcexp}.

We first present some cryptographic primitives that will be used in the rest of this paper (\cref{sec:design:crypto}).
Then, we give an overview of our design (\cref{sec:design:overview}) followed by the elaboration of its most interesting aspects (\crefrange{sec:design:keys}{sec:design:localcomm}).
We conclude this \lcnamecref{sec:design} with an end-to-end example (\cref{sec:design:example}).

\subsection{Cryptographic Primitives}
\label{sec:design:crypto}

Throughout the design of Sancus, we assume the existence of three cryptographic primitives.
First, a classical cryptographic hash function is used to compute digests of data.
Second, a \emph{key derivation function} is used to derive a cryptographic key from a master key and some diversification data:
\[
  K_{M,D} = \kdf(K_M, D)
\]

Third, an \emph{authenticated encryption with associated data} primitive is used to simultaneously provide confidentiality, integrity, and authenticity guarantees on data.
Such a primitive consists of two functions: one for encryption and one for decryption.
The encryption function takes as input a key, plaintext and associated data and produces ciphertext and an authentication tag.
The ciphertext covers the given plaintext and the tag is a \ac{MAC} over both the plaintext and the associated data:
\[
  C, T = \aee(K, P, A)
\]
The decryption function does the opposite operation and fails (i.e., produces no plaintext) if the tag is incorrect for the given ciphertext and associated data:
\[
  P = \aed(K, C, A, T)
\]
Note that this primitive can be used to compute a plain \ac{MAC} over some data:
\[
  \mac(K, D) \equiv \aee(K, \{\}, D)
\]
(Here, we discard the ciphertext result of $\aee$.)

\subsection{Overview}
\label{sec:design:overview}

\subsubsection{Nodes}
Nodes are low-cost, low-power microcontrollers (our implementation is based on the TI MSP430).
The processor in a node uses a Von Neumann architecture with a single address space for instructions and data.
To distinguish actual nodes belonging to $\IP$ from fake nodes set up by an attacker, $\IP$ shares a symmetric key with each of its nodes.
We call this key the \emph{node master key}, and use the notation $K_N$ for the node master key of node $N$.
Given our attacker model where the attacker can control all software on the nodes, it follows that this key must be managed by the hardware, and it is only accessible to software in an indirect way.

\subsubsection{Software Providers}
Software providers are principals that can deploy software to the nodes of $\IP$.
Each software provider has a unique public ID $\SP$.%
\footnote{Throughout this text, we will often refer to a software provider using its ID $\SP$.}
$\IP$ uses a key derivation function  $\kdf$ to  compute a key $K_{N,\SP} = \kdf(K_N,\SP)$, which $\SP$ will later use to setup secure communication with its modules.
Since node $N$ has key $K_N$, nodes can compute $K_{N,\SP}$ for any $\SP$.
The node will include a hardware implementation of $\kdf$ so that the key can be computed without trusting any software.

\subsubsection{Software Modules}
Software modules are essentially simple binary files containing two mandatory sections: a \emph{text section} containing code and constants and a \emph{data section} containing a module's runtime data.
As we will see later, the contents of the latter section are not attested and are therefore vulnerable to malicious modification before hardware protection is enabled.
Therefore, the processor will zero-initialize its contents at the time the protection is enabled to ensure an attacker can not have \emph{any} influence on a module's initial state.
Next to the two protected sections discussed above, a module can opt to load a number of \emph{unprotected sections}.
This is useful to, for example, limit the amount of code that can access protected data.
Indeed, allowing code that does not need it access to protected data increases the possibility of bugs that could leak data outside of the module.
In other words, this gives developers the opportunity to keep the trusted code of their own modules \emph{as small as possible}.
Each section has a header that specifies the start and end address of the section.

The \emph{identity} of a software module consists of a hash of
\begin{paraenum}
  \item the content of the text section and
  \item the start and end addresses of the text and data sections.
\end{paraenum}
We refer to this second part of the identity as the \emph{layout} of the module.
It follows that two modules with the exact same code and data can coexist on the same node and will have different identities as their layout will be different.
We will use notations such as $\SM$ or $\SM_1$ to denote the identity of a specific software module.

Software modules are always loaded on a node on behalf of a specific software provider $\SP$.
A software module is deployed by loading each of the sections of the module in memory at the specified addresses.
For each module, the processor maintains the layout information in a \emph{protected storage} area inaccessible from software.
It follows that the node can compute the identity of all modules loaded on it: the layout information is present in protected storage and the content of the text section is in memory.

An important sidenote here is that the loading process is \emph{not} trusted.
It is possible for an attacker to intervene and modify the module during loading.
However, this will be detected as soon as the module communicates with its provider (\Cref{sec:design:attest}).

Finally, the node computes a symmetric key $K_{N,\SP,\SM}$ that is specific to the module $\SM$ loaded on node $N$ by provider $\SP$.
It does so by first computing  $K_{N,\SP} = \kdf(K_N,\SP)$ as discussed above, and then computing  $K_{N,\SP,\SM} = \kdf(K_{N,\SP},\SM)$.
All these keys are kept in the protected storage, and will only be available to software indirectly by means of new processor instructions we discuss later.
\Cref{flt:keys} gives an overview of the keys used by Sancus.

\begin{table}
  \caption{%
    Overview of the keys used in Sancus, how they are created and who can access them.
    Note that derived keys are also accessible by any entity that has access to their master keys but this is not explicitly mentioned.
  }
  \begin{tabular}{lll}
    \toprule
    Key             & Creation               & Accessible by      \\
    \midrule
    $K_N$           & Random                 & $\IP$, $N$         \\
    $K_{N,\SP}$     & $\kdf(K_N, \SP)$       & $\SP$              \\
    $K_{N,\SP,\SM}$ & $\kdf(K_{N,\SP}, \SM)$ & $\SM$ (indirectly) \\
    \bottomrule
  \end{tabular}
  \label{flt:keys}
\end{table}

Note that the provider $\SP$ can also compute $K_{N,\SP,\SM}$, since he received $K_{N,\SP}$ from $\IP$ and since he knows the identity $\SM$ of the module he is loading on $N$.
This key will be used to attest the presence of $\SM$ on $N$ to $\SP$ and to secure the communications between $\SM$ and $\SP$.

\begin{figure*}
  \centering
  \resizebox{\linewidth}{!}{\input{images/node.tex}}
  \caption{
    A node with a software module loaded.
    The left part of the protected storage area is global while the right part is per module metadata.
    Sancus ensures the keys can never leave the protected storage area by only making them available to software in indirect ways through new processor instructions.
  }
  \label{flt:smlayout}
\end{figure*}

\Cref{flt:smlayout} shows a schematic of a node with a software module loaded. The picture also shows the keys and the layout information that the node has to manage.

\subsubsection{Memory Protection on the Nodes}
The various modules on a node must be protected from interfering with each other in undesired ways by means of some form of memory protection.
Our design relies on \emph{program counter-based memory access control}~\cite{KULeuven-277417}, as this memory access control model has been shown to support strong isolation~\cite{fides}, as well as remote attestation~\cite{smart}.
Roughly speaking, isolation is implemented by restricting access to the data section of a module such that it is only accessible while the program counter is in the corresponding text section of the same module.
Moreover, the processor instructions that use the keys $K_{N,\SP,\SM}$ will be program counter-dependent.
Essentially, the processor offers special instructions to access the cryptographic capabilities.
If such an instruction is invoked from within the text section of a specific module $\SM$, the processor will use key $K_{N,\SP,\SM}$.
Moreover, these instructions are only available after memory protection has been enabled for module $\SM$.
It follows that only a well-isolated $\SM$ installed on behalf of $\SP$ on $N$ can compute cryptographic primitives with $K_{N,\SP,\SM}$, and this is the basis for implementing both remote attestation and secure communication.

\subsubsection{Remote Attestation and Secure Communication}
In order to provide a confidential, integrity-protected, and authenticated communication channel between a software provider and its modules, Sancus includes an authenticated encryption primitive.
New instructions are provided to encrypt and decrypt messages using the key of the calling module.
When $\SP$ receives a message encrypted with $K_{N,\SP,\SM}$, he will have high assurance that it has been produced by $\SM$ since, as mentioned above, only $\SM$ is able to use this key.
Note that since $\SM$ is the \emph{identity} of the module that $\SP$ is communicating with, this primitive also provides for remote attestation.

\subsubsection{Secure Linking}
A final aspect of our design is how we deal with secure linking.
When a software provider sends a module $\SM_1$ to a node, this module can specify that it wants to link to another module $\SM_2$ on the same node, so that $\SM_1$ can call services of $\SM_2$ locally.
$\SM_1$ specifies this by including the identity (i.e., a hash) of $\SM_2$ in its text section.%
\footnote{Note that if $\SM_2$ also wants to link to $\SM_1$, this method creates a circular dependency between their identities.
  This can be resolved by not including the other's identity in the text section but having the software provider securely send it after deployment and storing it in the data section.}
The processor includes a new instruction that $\SM_1$ can call to check that
\begin{paraenum}
  \item there is a module loaded (with memory protection enabled) at the address of $\SM_2$ and
  \item the identity of that module has the expected value.
\end{paraenum}

A similar mechanism can be used by $\SM_2$ to verify that it is indeed called by $\SM_1$ (\emph{caller authentication}).
In its entry point, $\SM_2$ can call a new instruction that verifies the identity of the module that called the entry point.
For this to work, the processor keeps track of the \emph{previously} executing software module.

Fortunately, this expensive -- a hash needs to be calculated over a potentially large text section -- authentication of software modules is needed only once.
\Cref{sec:design:localcomm} discusses a more efficient procedure for subsequent authentications.

\subsection{Key Management}
\label{sec:design:keys}

We handle key management without relying on public-key cryptography~\cite{micali}.
$\IP$ is a trusted authority for key management.
All keys are generated and/or known by $\IP$.
There are three types of keys in our design (\cref{flt:keys}):
\begin{itemize}
  \item Node master keys $K_N$ shared between node $N$ and~$\IP$.
  \item Software provider keys $K_{N,\SP}$ shared between a provider $\SP$ and a node $N$.
  \item Software module keys $K_{N,\SP,\SM}$ shared between a node $N$ and a provider $\SP$, and the hardware of $N$ makes sure that only $\SM$ can use this key.
\end{itemize}
We have considered various ways to manage these keys.
A first design choice is how to generate the node master keys.
We considered three options:
\begin{paraenum}
  \item\label{item:same-key} using the same node master key for every node,
  \item\label{item:random-key} randomly generating a separate key for every node using a secure random number generator and keeping a database of these keys at $\IP$, and
  \item\label{item:derived-key} deriving the master node keys from an $\IP$ master key using a key derivation function and the node identity $N$.
\end{paraenum}

We discarded option \ref{item:same-key} because for this choice the compromise of a single node master key breaks the security of the entire system, hence violating \emph{hardware breach confinement} (\cref{sec:problem:secprops}).
Options \ref{item:random-key} and \ref{item:derived-key} are both reasonable designs that trade off the amount of secure storage and the amount of computation at $\IP$'s site.
Our prototype uses option \ref{item:random-key}.

The software provider keys $K_{N,\SP}$ and software module keys $K_{N,\SP,\SM}$ are derived using a key derivation function as discussed in the overview section.

Finally, an important question is how compromised keys can be handled in our scheme.
Since any secure key derivation function has the property that deriving the master key from the derived key is computationally infeasible, the compromise of neither a module key $K_{N,\SP,\SM}$ nor a provider key $K_{N,\SP}$ needs to lead to the revocation of $K_N$.
If $K_{N,\SP}$ is compromised, provider $\SP$ should receive a new name $\SP'$ since an attacker can easily derive $K_{N,\SP,\SM}$ for any $\SM$ given $K_{N,\SP}$.
If  $K_{N,\SP,\SM}$  is compromised, the provider can still safely deploy other modules.
$\SM$ can also still be deployed if the provider makes a change to the text section of $\SM$.%
\footnote{For example, a random byte could be appended to the text section without changing the semantics of the module.}
If $K_N$ is compromised, it needs to be revoked.
Since $K_N$ is different for every node, this means that only one node needs to be either replaced or have its key updated.

\subsection{Memory Access Control}
\label{sec:design:isolation}

Memory can be divided into
\begin{paraenum}
  \item memory belonging to modules, and
  \item the rest, which we refer to as unprotected memory.
\end{paraenum}
Memory allocated to modules is divided into two sections, the text section, containing code and constants, and the data section containing  all the data that should remain confidential and should be integrity protected.
Modules can also have an unprotected data section that is considered to be part of unprotected memory from the point of view of the memory access control system.

Apart from application-specific data, runtime metadata such as the module's call stack should typically also be included in the data section.
Indeed, if a module's stack were to be shared with untrusted code, confidential data may leak through stack variables or control-data might be corrupted by an attacker.
It is the module's responsibility to make sure that its call stack and other runtime metadata is in its data section, but our implementation comes
with a compiler that ensures this automatically (\cref{sec:implementation:compiler}).

The memory access control logic in the processor enforces that
\begin{paraenum}
  \item the data section of a module is only accessible while code in the text section of that module is being executed,
  \item the text section can only be executed by jumping to a well-defined entry point, and
  \item the text section cannot be written and can only be read while code in that section is being executed.
\end{paraenum}
The second part is  important since it prevents attackers from misusing code chunks in the text section to extract data from the data section.
For example, without this guarantee, an attacker might be able to launch a \ac{ROP} attack~\cite{Castelluccia:2009:DSA:1653662.1653711} by selectively combining gadgets found in the text section.
Of course, if a module contains a bug that allows an attacker to divert its control-flow, he might still be able to launch such an attack; enforcing an entry point prevents these attacks being launched from code outside of the module.
Note that, as shown in \cref{flt:smlayout}, our design allows modules to have a single entry point only.
This may seem like a restriction but, as we will show in \cref{sec:implementation:compiler}, it is not since multiple logical entry points can easily be dispatched through a single physical entry point.
\Cref{flt:access_rights} gives an overview of the enforced access rights.

\begin{table}
  \centering
  \def\r#1{\texttt{#1}}
  \setlength{\tabcolsep}{4pt}
  \caption{%
    Memory access control rules enforced by Sancus using the traditional Unix notation.
    Each entry indicates how code executing in the ``from'' section may access the ``to'' section.
  }
  \begin{tabular}{lcccc}
    \toprule
    From/to     & Entry   & Text    & Data    & Unprotected  \\
    \midrule
    Entry       & \r{r-x} & \r{r-x} & \r{rw-} & \r{rwx}      \\
    Text        & \r{r-x} & \r{r-x} & \r{rw-} & \r{rwx}      \\
    Unprotected/
    \\Other SM  & \r{--x} & \r{---} & \r{---} & \r{rwx}      \\
    \bottomrule
  \end{tabular}
  \label{flt:access_rights}
\end{table}

Besides memory access control, the processor also ensures that modules cannot be interrupted while being executed to prevent register contents from leaking outside the module.
Supporting interruptible modules is orthogonal to our goals and is left as future work.

Memory access control for a module is enabled at the time the module is loaded.
First, untrusted code (for instance the node's operating system) will load the module in memory as discussed in \cref{sec:design:overview}.
Then, a special instruction is issued:
\begin{center}
  \instr{protect} \instrarg{layout, $\SP$}
\end{center}
\label{instr:protect}
This processor instruction has the following effects:
\begin{itemize}
  \item the layout is checked not to overlap with existing modules, and a new module is registered by storing the layout information in the protected storage of the processor (\cref{sec:design:overview,flt:smlayout});
  \item memory access control is enabled as discussed above; and
  \item the module key $K_{N,\SP,\SM}$ is created -- using the text section and layout of the actually loaded module -- and stored  in the protected storage.
\end{itemize}
This explains why we do not need to trust the \ac{OS} that loads the module in memory: if the content of the text section or the layout would be modified before execution of the \instr{protect} instruction, then the key generated for the module would be different, and subsequent attestations or authentications performed by the module would fail.
Once the \instr{protect} instruction has succeeded, the hardware-implemented memory access control scheme ensures that software on the node can no longer tamper with $\SM$.

The only way to lift the memory access control is by calling the processor instruction:
\begin{center}
  \instr{unprotect} \instrarg{continuation}
\end{center}
The effect of this instruction is to lift the memory protection of the module \emph{from which the} \instr{unprotect} \emph{instruction is called}.
To prevent the leakage of confidential data, this instruction also clears the module's code- and data sections.
Since the \instr{unprotect} instruction itself is part of the code section, the programmer has to provide a pointer to the code where the execution is to be continued in the \instrarg{continuation} argument.

Finally, it remains to decide how to handle memory access violations.
We opt for the simple design of resetting the processor and clearing memory on every reset.
This has the advantage of clearly being secure for the security properties we aim for.
However an important disadvantage is that it may have a negative impact on availability of the node: a bug in the software may cause the node to reset and clear its memory.
An interesting avenue for future work is to come up with strategies to handle memory access violations in less severe ways.
Invalid reads could return some default value as in secure multi-execution~\cite{sme}.
Invalid writes or jumps could be dropped or modified to actions that are allowed as in edit-automata~\cite{polymer}.
For instance, an invalid memory read might just return zero, and an invalid jump might be redirected to an exception handler.

\subsection{Remote Attestation and Secure Communication}
\label{sec:design:attest}

We extend the processor with two more instructions that are used for remote attestation and secure communication:
\begin{center}
  \instr{encrypt} \instrarg{plaintext, associated data, ciphertext (output), tag (output)[, key]} \\
  \instr{decrypt} \instrarg{ciphertext, associated data, tag, plaintext (output)[, key]}
\end{center}
These instructions have the same semantics as the $\aee$ and $\aed$ functions, respectively (\cref{sec:design:crypto}).

As can be seen from the signatures above, both instructions have the key as an optional argument.
If none is given, the module key of the invoking module is implicitly used (or an error code is returned if invoked by unprotected code).
This is the \emph{only} way for software to access a module key and the key $K_{N,\SP,\SM}$ will \emph{only} be used when invoked by the module with identity $\SM$ deployed by $\SP$ on node $N$.
Note that, besides being able to access the module key, these instructions are \emph{not} privileged and the same memory access rules are enforced as for any instruction that accesses memory.

These instructions can be used to provide confidentiality, integrity, and authenticity guarantees of data exchanged between modules and their providers.
The ciphertext plus the corresponding tag can be sent using the untrusted operating system over an untrusted network.
If the tag verifies correctly (using $K_{N,\SP,\SM}$) upon receipt by the provider $\SP$, he can be sure that the decrypted plaintext indeed comes from $\SM$ running on $N$ on behalf of $\SP$ as the node's hardware makes sure only this specific module can use the module key $K_{N,\SP,\SM}$.
The reasoning is equivalent for data sent to the module.

To implement remote attestation, we only need to add a freshness guarantee (i.e., protect against replay attacks). Provider $\SP$ sends a fresh nonce $\No$ to the node $N$,
and the module $\SM$ returns the MAC of this nonce using the key $K_{N,\SP,\SM}$, computed with the \instr{encrypt} instruction (\cref{sec:design:crypto} explains how this can be done).
This gives the $\SP$ assurance that the correct module is running on that node at this
point in time.

Building on this scheme, we can also implement  secure communication.
Whenever $\SP$ wants to receive data from $\SM$ on $N$, it sends a request to the node containing a nonce $\No$ and possibly some input data $I$ that is to be provided to $\SM$.
This request is received by untrusted code on the node which passes $\No$ and $I$ as arguments to the function of $\SM$ to be called.
When $\SM$ has calculated the output $O$, it asks the processor to calculate $\aee(K_{N,\SP,\SM}, O, \No || I)$ using the \code{encrypt} instruction.
The resulting ciphertext and tag are then sent to $\SP$.
By verifying the tag with its own copy of the module key, the provider has strong assurance that $O$ has been produced by $\SM$ on node $N$ given input $I$.

$\SP$ can use secure communication to establish a shared secret between two or more of its modules to allow them to directly communicate with each other.
Although this is feasible for modules running on different nodes, the overhead is probably too high for secure communication between modules running on the same node; we discuss a more efficient technique in \cref{sec:design:localcomm}.

\subsection{Confidential Loading}
\label{sec:design:confidential-loading}

If, besides the integrity guarantees provided through remote attestation, one wants to have \emph{confidentiality guarantees} for a module's text section, more architectural support is necessary.
Indeed, although a module's text section is not readable by other modules (\cref{flt:access_rights}), this is only enforced after enabling a module; i.e., up to that point an attacker can easily read the module's text section.

Therefore, we provide a second way to use the \code{protect} instruction:
\begin{center}
  \code{protect} \textit{layout}, $\SP$, \textit{MAC}
\end{center}
In this form, the \code{protect} instruction behaves exactly the same (\cref{instr:protect}) except that, before calculating the module key, the module's text section is decrypted in place using $K_{N,\SP}$. If the integrity check using the given \ac{MAC}, i.e., an authenticated encryption tag, fails, the text section is cleared and the protection disabled.

Note that $K_{N,\SP}$ is now used to encrypt confidential modules, as well as for key derivation.
However, the uniqueness of both operations can be guaranteed by domain separation, i.e., by setting the first bit of the associated data to $0$ or $1$ for the encryption and key derivation respectively.

It should be mentioned that the integrity check is not strictly necessary for confidential loading, since any subsequent remote attestation will also verify the module's integrity.
However, it could be used as a simple form of module authentication: by disabling the non-decrypting form of the \code{protect} instruction, only entities possessing a valid software provider key can install modules on the system.

\subsection{Secure Linking and Local Communication}
\label{sec:design:localcomm}

In this \lcnamecref{sec:design:localcomm}, we discuss how we assure the secure linking property mentioned in \cref{sec:problem:secprops}.
More specifically, we consider the situation where a module $\SM_1$ wants to call another module $\SM_2$ and wants to be ensured that
\begin{paraenum}
  \item the integrity of $\SM_2$ has not been compromised, and
  \item $\SM_2$ is correctly protected by the processor.
\end{paraenum}

In our design, if module  $\SM_1$ wants to link securely to $\SM_2$, $\SM_1$ should be deployed with the identity of $\SM_2$.
The processor provides a special instruction to check the existence and integrity of a module at a specified address:
\begin{center}
  \instr{attest} \instrarg{address, expected hash}
\end{center}
This instruction will:
\begin{itemize}
\item verify that a module is loaded (with protection enabled) at the provided address;
\item compute the identity of that module (i.e., a hash of its text section and layout);
\item compare the resulting hash with the \instrarg{expected hash} parameter of the instruction; and
\item if the hashes were equal, return the module's ID (to be explained below), otherwise return zero.
\end{itemize}

Using this processor instruction, a module can securely check for the presence of another expected module, and can then call that other module.

Since this authentication process is relatively expensive (it requires the computation of a hash), our design also includes a more efficient mechanism
for repeated authentication. The processor will assign sequential IDs%
\footnote{To avoid confusion between the two different identity concepts used in this text, we will refer to the hardware-assigned number as \emph{ID} while the text section and layout of a module is referred to as \emph{identity}.}
to modules that it loads, and will ensure that -- within one boot cycle --
it never reuses these IDs.
This can be implemented by storing the ID to be used for the next module in a register (``Next ID'' in \cref{flt:smlayout}), incrementing it after a new module is enabled, and generating a violation when it overflows.
A processor instruction:
\begin{center}
  \instr{get-id} \instrarg{address}
\end{center}
checks that a protected module is present at \instrarg{address} and returns the ID of the module. Once a module has checked using the initial authentication
method that the module at a given address is the expected module, it can remember the ID of that module, and then for subsequent authentications
it suffices to check that the same module is still loaded at that address using the \instr{get-id} instruction.

For caller authentication, the processor keeps track of the previously executing module by recording its ID in a register (``Caller ID'' in \cref{flt:smlayout}).
This register is updated whenever execution enters a new module.
Modules can attest their caller through two instruction: \instr{attest-caller} and \instr{get-caller-id}.
These instruction behave similar to \instr{attest} and \instr{get-id} respectively but use the previously executing module implicitly.

\subsection{An End-to-End Example}
\label{sec:design:example}

To make the discussion in the previous \lcnamecrefs{sec:design:example} more concrete, this \lcnamecref{sec:design:example} gives a small example of how
our design may be applied in the area of sensor networks. \Cref{flt:example_setup} shows our example setup.
It contains a single node to which a sensor $S$ is attached; communication with $S$ is done through memory-mapped I/O.
The owner of the sensor network, $\IP$, has deployed a special module, $\SM_S$, that is in charge of communicating with $S$.
By ensuring that the data section of $\SM_S$ contains the memory-mapped I/O region of $S$, $\IP$ ensures that no software outside
of $\SM_S$ is allowed to configure or communicate directly with $S$; all requests to~$S$ need to go through $\SM_S$.

\Cref{flt:example_setup} also shows a number of software providers ($\SP_1,\ldots,\SP_n$) who have each deployed a module ($\SM_1,\ldots,\SM_n$).
In the remainder of this \lcnamecref{sec:design:example}, we walk the reader through the life cycle of a module in this example setup.

\begin{figure}
  \centering
  \begin{tikzpicture}[remember picture, node distance=50]
    \node[sm] (MS) {$\SM_S$};
    \node[sm, above right of=MS] (M1) {$\SM_1$};
    \node[sm, below right of=MS] (MN) {$\SM_n$};
    \begin{pgfonlayer}{background}
      \node[processor, fit=(MS) (M1) (MN), inner sep=10, label=Node] {};
    \end{pgfonlayer}
    \node[draw, left of=MS] (S) {$S$};
    \node[sp, right of=M1] (P1) {$\SP_1$};
    \node[sp, right of=MN] (PN) {$\SP_n$};
    \node[ip, below right of=P1] (IP) {$IP$};
    \draw[<->] (MS) -- (S);
    \draw[<->] (MS) -- (M1);
    \draw[<->] (MS) -- (MN);
    \draw[<->] (M1) -- (P1);
    \draw[<->] (MN) -- (PN);
    \draw[<->] (P1) -- (IP);
    \draw[<->] (PN) -- (IP);
    \path (M1) -- (MN) node[midway] {\Large\vdots};
    \path (P1) -- (PN) node[midway] {\Large\vdots};
  \end{tikzpicture}
  \caption{
    Setup of the sensor node example discussed in \cref{sec:design:example}.
    Sancus ensures only module $\SM_S$ is allowed to directly communicate with the sensor $S$.
    Other modules securely link to $\SM_S$ to receive sensor data in a trusted way.
  }
  \label{flt:example_setup}
\end{figure}

The first step for a provider $\SP$ is to contact $\IP$ and request permission to run a module on the sensor node.
If $\IP$ accepts the request, it provides $\SP$ with its provider key for the node, $K_{N,\SP}$.

Next, $\SP$ creates the module $\SM$, that he wants to run on the processor and calculates the associated module key, $K_{N,\SP,\SM}$.
Since $\SM$ will communicate with $\SM_S$, $\SP$ requests the identity of $\SM_S$ from $\IP$.
This identity is included in the text section of $\SM$, so that $\SM$ can use it to authenticate $\SM_S$.
Then $\SM$ is sent to the node for deployment.

Once $\SM$ is received on the node, it is loaded, by untrusted software like the operating system, into memory and the processor is requested to protect $\SM$, using the
\verb+protect+ processor instruction. As discussed, the processor enables memory protection, computes the key $K_{N,\SP,\SM}$,
and stores it in hardware.

Now that $\SM$ has been deployed, $\SP$ can start requesting data from it.
We will assume that $\SM$'s function is to request data from $S$ through $\SM_S$, perform some transformation, filtering, or aggregation on it, and return the result to $\SP$.
The first step is for $\SP$ to send a request containing a nonce $\No$ to the node.
Once the request is received (by untrusted code) on the node, $\SM$ is called passing $\No$ as an argument.

Before $\SM$ calls $\SM_S$, it needs to verify the integrity of module $\SM_S$.
It does this by executing the \instr{attest} instruction passing the address of the expected identity of $\SM_S$ (included in $\SM$'s text section) and the address of the entry point it is about to call.
The ID of $\SM_S$ is then returned to $\SM$ and, if it is non-zero, $\SM$ calls $\SM_S$ to receive the sensor data from $S$. $\SM$ will usually also store the returned ID of $\SM_S$ in its data section so that future authentications of $\SM_S$ can be done with the \verb+get-id+ instruction.

Once the received sensor data has been processed into the output data $O$, $\SM$ will request the processor to calculate $\aee(K_{N,\SP,\SM}, O, \No)$ using the \instr{encrypt} instruction.
$\SM$ then passes the ciphertext $C$ and tag $T$ to the (untrusted) network stack to be sent to $\SP$.
When $\SP$ receives the output of $\SM$, it can verify its integrity by calculating $\aed(K_{N,\SP,\SM}, C, \No, T)$.
