
TEXFILES = applications.tex conclusion.tex design.tex evaluation.tex \
	   implementation.tex introduction.tex problem.tex related-work.tex


ISPELLPAT= -ktexskip1 +cref,Cref,url


all:
	latexmk -pdf paper.tex

spelling:
	ispell -t ${ISPELLPAT} -b -d american -p ./paper.dict ${TEXFILES}
	ispell -o -d american -p ./paper.dict -F ./submission/deformat-response.py \
           submission/response.txt

clean:
	latexmk -C -pdf paper.tex
	rm -f paper.bbl paper.synctex.gz

