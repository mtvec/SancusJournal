#include "common.h"

#include <sancus/sm_support.h>
#include <sancus_support/tsc.h>

#include <stdio.h>

#define CYCLES_TO_WORK 0

inline void __attribute__((always_inline)) delay_cycles(tsc_t cycles)
{
    tsc_t start = tsc_read();

    while (tsc_read() - start <=  cycles) {}
}

SM_DATA(sensor) int sensor_value;

SM_ENTRY(sensor) int read_sensor_data()
{
    return sensor_value;
}

DECLARE_SM(sensor, 0x1234);

static int processed_result;
static uint8_t processed_result_tag[SANCUS_TAG_SIZE];

SM_ENTRY(processor) void process_sensor_data(unsigned nonce)
{
    int result = read_sensor_data();

#if CYCLES_TO_WORK > 0
    delay_cycles(CYCLES_TO_WORK);
#endif

    sancus_wrap(&nonce, sizeof(nonce), &result, sizeof(result),
                &processed_result, processed_result_tag);
}

DECLARE_SM(processor, 0x1234);

DECLARE_TSC_TIMER(timer);

static int ref_sensor_value;

static int __attribute__((noinline)) ref_read_sensor_data()
{
    return ref_sensor_value;
}

static int __attribute__((noinline)) ref_process_sensor_data()
{
    int result = read_sensor_data();

#if CYCLES_TO_WORK > 0
    delay_cycles(CYCLES_TO_WORK);
#endif

    return result;
}

int main()
{
    init_hardware();
    sancus_enable(&sensor);
    sancus_enable(&processor);

    const unsigned nonce = 0x5678;

    printf("Timing macro benchmark with %u cycles of work\n", CYCLES_TO_WORK);
    puts("Sancus run 1/2");

    TSC_TIMER_START(timer);
    process_sensor_data(nonce);
    TSC_TIMER_END(timer);

    puts("Sancus run 2/2");

    TSC_TIMER_START(timer);
    process_sensor_data(nonce);
    TSC_TIMER_END(timer);

    puts("Reference run");

    TSC_TIMER_START(timer);
    ref_process_sensor_data();
    TSC_TIMER_END(timer);

    puts("======================================================");
}
