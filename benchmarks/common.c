#include "common.h"

#include <msp430.h>

#include <sancus_support/uart.h>

void init_hardware()
{
    WDTCTL = WDTHOLD | WDTPW;
    uart_init();
}

int putchar(int c)
{
    if (c == '\n')
        putchar('\r');

#ifdef FPGA
    uart_write_byte(c);
#else
    P1OUT = c;
    P1OUT |= 0x80;
#endif
    return c;
}
