\section{Introduction}
Computing devices and software are omnipresent in our society, and society increasingly relies on the correct and secure functioning of these devices and software.
Two important trends can be observed.
First, network connectivity of devices keeps increasing.
More and more (and smaller and smaller) devices get connected to the Internet or local ad-hoc networks. Many consumer products contain embedded technology to have Internet connectivity. This \ac{IoT} is estimated to grow to an astonishing number of 26 billion units by 2020~\cite{iot-growth-gartner}.
Second, more and more devices support extensibility of the software they run -- often even by third parties different from the device manufacturer or device owner. The \ac{IoT} becomes {\em infrastructure} on which many stakeholders
can install and run software applications.
These two factors are important because they enable a vast array of interesting applications, ranging from over-the-air updates on smart cards, over updatable implanted medical devices, to programmable sensor networks or smart home applications.
However, these two factors also have a significant impact on security threats.
The combination of connectivity and software extensibility leads to malware threats.
Researchers have already shown how to perform code injection attacks against embedded devices to build self-propagating worms~\cite{Giannetsos:2009:SWW:1658997.1659015,Francillon:2008:CIA:1455770.1455775}.
\citeN{6322974} describe several recent incidents and summarize the state of embedded device security as ``a mess''.

For high-end devices, such as servers or desktops, the problems of dealing with connectivity and software extensibility are relatively well-understood, and there is a rich body of knowledge built up from decades of research; we provide a brief survey in the related work section.

However, for low-end, resource-constrained devices, no effective low-cost solutions are known.
Many embedded platforms lack standard security features present in high-end processors, such as privilege levels or advanced memory management units that support virtual memory.
Depending on the overall system security goals, as well as the context in which the system must operate, there
may be more optimal solutions than just porting the general-purpose security features from high-end
processors.

Over the past few years, researchers have been exploring alternative security architectures for low-end networked devices.
For instance, \citeN{smart} propose SMART, a simple and efficient hardware-software primitive to establish a dynamic root of trust in an embedded processor, and \citeN{KULeuven-277417} propose a simple program counter-based memory access control system to isolate software components. For a more complete overview of this line of work, we refer to~\Cref{sec:related}.

The key contribution of this paper is the design, implementation and evaluation of one such security architecture, the Sancus architecture. Sancus was first proposed in 2013 at the USENIX Security conference \cite{sancus} as
a security architecture that supports secure third-party software extensibility for a network of low-end processors with a hardware-only \ac{TCB}.
Over the past three years, significant experience has been gained with
applications of Sancus, including for instance the development of a trust assessment infrastructure that uses Sancus to protect the trust measurement code \cite{muehlber:sancus-ta}, and the design of a smart meter secured by Sancus \cite{muehlber:smart_meter}. Also, researchers have been investigating several extensions of Sancus, for instance to support more flexible resource sharing \cite{vanbulck:sancus_ac} or to support confidential loading of code \cite{goetzfried2015}.
Informed by these additional research results, this journal version of the Sancus paper describes an improved design and implementation, supporting additional security guarantees, such as confidential deployment and a more efficient cryptographic core.
While this paper extends and improves the conference paper, we carefully made sure that this paper is self-contained: readers do not have to be familiar with the conference paper.

More specifically, we make the following contributions:
\begin{itemize}
\item We propose Sancus\footnote{Sancus was the ancient Roman god of trust, honesty and oaths.}, a security architecture for resource-constrained, extensible networked embedded systems, that can provide strong isolation guarantees, remote attestation, secure communication, secure linking, and confidential software deployment with a minimal (hardware) \ac{TCB}.
\item We implement the hardware required for Sancus as an extension of a mainstream microprocessor, and we show that the cost of these hardware changes (in terms of performance, area, and power) is small.
\item We implement a C compiler that targets Sancus-enabled devices.
  Building software modules for Sancus can be done using simple annotations with standard C code, showing that the cost in terms of software development is low as well.
\item We implement a Contiki-based (untrusted) software stack to automate the deployment process of Sancus modules.
\item We report on our experience with implementing a variety of applications on Sancus, and evaluate Sancus in terms of performance, hardware cost, and security.
\end{itemize}
To guarantee the reproducibility and verifiability of our results, all our research materials, including the hardware design of the processor, the C compiler, and the deployment stack are publicly available.

The remainder of this paper is structured as follows.
First, in \cref{sec:problem} we clarify the problem we address by defining our system model, attacker model, and the security properties we aim for.
The next two sections detail the design of Sancus and some interesting implementation aspects. \Cref{sec:apps} describes applications that have been developed with Sancus, and outlines remaining challenges for future work.
\Cref{sec:evaluation} reports on our evaluation of Sancus and the final two sections discuss related work and conclude.
