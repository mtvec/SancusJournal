\section{Evaluation}
\label{sec:evaluation}

In this \lcnamecref{sec:evaluation} we  evaluate Sancus in terms of runtime performance, impact on chip size, and provided security.
All experiments were performed using a Xilinx XC6SLX25 Spartan-6 FPGA with
a speed grade of $-2$, synthesized using Xilinx ISE Design Suite.

\subsection{Performance}
\label{sec:evaluation:performance}

There are two important performance aspects to consider with our design.
First, since we made changes to the CPU core, we evaluate the impact on its critical path, i.e., the maximum frequency it can run at.
Second, we measure the runtime overhead of the added instructions, as well as the code transformations performed by the compiler.

\subsubsection{Critical Path}

Since the Xilinx tools offer no direct way to find the critical path of a design, we measured it indirectly.
Using timing constraints, one can specify what clock rate certain signals should be able to sustain; the tools will then err when the constraint cannot be met.
By varying the constraint on the input clock signal, we can get a measure on how fast the design will be able to run and, thus, on the length of the critical path.
%
We found that the unmodified \omsp{} core can run at 51~MHz with our setup.
For our modifications, there are two parameters that may influence the critical path: the security level (i.e., the size of the keys) and the number of supported modules $N_{\SM}$.
The reason these parameters may influence the critical path is the same for both.
The keys are stored in the MAL circuits and routed to the crypto unit through a multiplexer.
Both the key size and $N_{\SM}$ will increase the size of this multiplexer, and hence increase the length of the critical path.

\Cref{flt:max_freq} shows the maximum obtainable frequency in function of $N_{\SM}$ for a number of different security levels.
Note that although our implementation allows for security levels up to 256 bits, 128 bits are ample for our target platforms, and we therefore do not evaluate higher security levels.

The influence of the security level and $N_{\SM}$ should be clear from the \lcnamecref{flt:max_freq}.
However, it should also be clear that the maximum frequency for any security level is not influenced much by values of $N_{\SM}$ up to 8 where it is always around 39~MHz.
The reason for the maximum frequency for low numbers of $N_{\SM}$ being smaller than the unmodified core (51~MHz) is because of the MAL circuits (\cref{flt:sm_hw}), which add a number of comparators to the path of the memory address bus.

\begin{figure}
  \centering
  \maxsizebox{.7\textwidth}{!}{\input{graphs/max_speed.pgf}}
  \caption{%
    The maximum frequency for which the core can be synthesized in function of the number of modules ($N_{\SM}$) for a number of security levels.
    The maximum frequency decreases with $N_{\SM}$ due to the large multiplexer needed to get the module key out of the MAL circuits.
    This also explains why the maximum frequency decreases much faster when the key size is larger.
    Note that the unmodified \omsp{} core can be synthesized at 51 MHz.
  }
  \label{flt:max_freq}
\end{figure}

\subsubsection{Microbenchmarks}

To quantify the impact on performance of our extensions, we first performed
microbenchmarks to measure the cost of each new instruction.
To this end, we added a custom timestamp counter peripheral to the CPU core that allowed us to conveniently measure the amount of cycles passed since power up.
It should also be noted that all measurements are completely noiseless and thus accurate. Consequently, it is not necessary to calculate an average value over multiple measurements.

The \instr{get-id} and \instr{unprotect} instructions are very fast: they both take one clock cycle.
The other instructions perform cryptographic operations on their input, and hence their runtime cost depends linearly on the size of the input they handle.
Remember that all cryptographic operations are implemented using the same underlying primitive (\cref{sec:implementation:crypto}), which means their runtime cost is almost exactly the same.
The only exception is the \instr{enable} instruction when confidential loading is used.
In this case the underlying primitive is called twice: once for decryption and once for key derivation.
Its cost is therefore twice as big as the other instructions.
\Cref{flt:micro-performance} shows the measurements for the \instr{encrypt} instruction for a number of different security levels.

\begin{figure}
  \centering
  \maxsizebox{.7\textwidth}{!}{\input{graphs/micro.pgf}}

  \caption{%
    Execution time of the \instr{encrypt} instruction for a number of security levels.
    The cost of the other cryptographic instructions is similar with the exception of \instr{enable} for confidential loading, which is twice as big.
    Notice how the performance for the 80- and 96 bit security levels is almost equal because they use the same \spongent{} variant.
  }
  \label{flt:micro-performance}
\end{figure}

Note that the performance of the 80- and 96 bit implementations is almost the same.
The reason for this is that the performance of our crypto unit is determined by the underlying \spongent{} variant.
Recall that \spongent{} is defined in different variants and we select the correct variant for the required security level (\cref{sec:implementation:crypto}).
The 80- and 96 bit security levels require the same \spongent{} variant.

\subsubsection{Macrobenchmark}

To give an indication of the impact on performance in real-world scenarios, we
performed the following macro benchmark. We synthesized our processor with 128-bit keys
and configured it as in the example shown in \cref{flt:example_setup}.
We measured the time it takes from the moment a request arrives at the
node until the response is ready to be sent back.
More specifically, the following operations are timed:
\begin{paraenum}
  \item the request is passed, together with the nonce, to $\SM_i$;
  \item\label{item:sensordata} $\SM_i$ requests $\SM_S$ for sensor data;
  \item\label{item:transform} $\SM_i$ performs some transformation on the received data; and
  \item\label{item:sign} $\SM_i$ encrypts its output together with the nonce.
\end{paraenum}
The overhead introduced by Sancus is due to a call to \instr{attest} in step \labelcref{item:sensordata} and a call to \instr{encrypt} in step \labelcref{item:sign} as well as the entry and exit code introduced by the compiler.
Since this overhead is fixed, the amount of computation performed in step \labelcref{item:transform} will influence the \emph{relative overhead} of Sancus.
Note that the size of the text section of $M_S$ is 230 bytes, and that nonces and output data encrypted by $M_i$ both have a size of 16 bits.

We measured the fixed overhead to be $26,834$ cycles for the first time data is requested from the module.
Since the call to \instr{attest} in step \labelcref{item:sensordata} is not needed after the initial verification (\cref{sec:design:localcomm}), we also measured the overhead of any subsequent requests, which is $3,481$ cycles.
Given these values, the relative overhead can be calculated in function of the number of cycles used during the computation in step \labelcref{item:transform}.
The result is shown in \cref{flt:macro-performance}.

\begin{figure}
  \centering
  \maxsizebox{.8\textwidth}{!}{\input{graphs/macro.pgf}}

  \caption{%
    Although the absolute overhead Sancus imposes on our example setup (\cref{flt:example_setup}) is quite large, the relative overhead quickly drops when the modules perform some useful work.
    Notice how the subsequent runs are much faster than the first due to the optimization discussed in \cref{sec:design:localcomm}.
  }
  \label{flt:macro-performance}
\end{figure}

\subsection{Area}
\label{sec:evaluation:area}

We evaluated the area of our design using Synopsys Design Compiler v2013.12 with the UMC 130nm and NanGate 15nm standard-cell libraries.
The default ASIC synthesis settings for the openMSP were used, except for disabling clock gating and DFT insertion.
The unmodified \omsp{} core measures 11kGE and 15kGE, respectively, using these libraries.
The area of Sancus in function of the number of modules $N_{\SM}$ for a number of security levels is shown in \cref{flt:area}.
If computational overhead is of lesser concern, the area can be reduced by
computing the module key on the fly instead of storing it in registers.
Exploring other improvements is left as future work.

\begin{figure}
  \centering
  \maxsizebox{\textwidth}{!}{\input{graphs/area_asic.pgf}}

  \caption{%
    The area of the whole \omsp{} core with Sancus extensions when synthesizing for different security levels using the UMC 130nm and NanGate 15nm standard-cell libraries.
    Synthesis was done for a target clock frequency of 25MHz.
  }
  \label{flt:area}
\end{figure}

\subsection{Security}
We provide an informal security argument for each of the security properties Sancus aims for (see \cref{sec:problem:secprops}).
%
First,  {\em software module isolation} is enforced by the memory access control logic in the processor. Both the access control model as well as its implementation
are sufficiently simple to have a high assurance in the correctness of the implementation. Moreover, Agten et
al.~\cite{agten2012fullabstraction,fully-abstract-pma} have shown that
higher-level isolation properties (similar to isolation between Java components) can be achieved by compiling to a processor with program counter-dependent memory access control.
Sancus does {\em not} protect against vulnerabilities in the implementation of a module. If a module contains buffer overflows or other memory safety related vulnerabilities,
attackers can exploit them using well-known techniques~\cite{llsbe} to get unintended access to data or functionality in the module. Dealing with such vulnerabilities is
an orthogonal problem, and a wide range of countermeasures
for addressing them has been developed~\cite{csur}.

The security of \emph{remote attestation} and \emph{secure communication} follows from the following key observation: the computation of MACs with the module key is
only possible by a module with the correct identity running on top of a processor configured with the correct node key (and, of course, by the software provider of the
module). As a consequence, if an attacker succeeds in
completing a successful attestation or communication with the software provider, he must have done it with the help of the actual module. In other words, within our
attacker model, only API-level attacks against the module are possible, and it is indeed possible to develop modules that are vulnerable to such attacks, for instance
if a module offers a function to compute MACs with its module key on arbitrary input data. But if the module developer avoids such API-level attacks, the security of
Sancus against attackers conforming to our attacker model follows.

If a module has access to the correct identity of another module it wants to call, the security of \emph{secure linking} follows from the definition of the \instr{attest} instruction (\cref{sec:design:localcomm}).
Indeed, this instruction will only succeed if a module with the given identity is enabled at the given location.
This means that an attacker can only force the instruction to succeed by either
\begin{paraenum}
  \item loading the correct module; or
  \item constructing a different module with the same identity.
\end{paraenum}
The latter amounts to finding a hash collision, which our attacker model precludes.

The identity used for secure linking must not be stored in unprotected memory where an attacker can easily manipulate it.
There are two options to provide the identity securely to a module.
First, it can be stored in a module's text section.
Although, if confidential loading is not used for this module, an attacker can manipulate the text section before protection is enabled,%
this manipulation will be detected when its provider performs remote attestation.
Second, the identity can be sent using secure communication after
deployment and stored in the module's data section.
This is the technique that our implementation uses (\cref{sec:implementation:deployment}).

The security of \emph{confidential loading} follows from two observations.
First, before the \instr{enable} instruction is called, the module's text section is encrypted using the vendor key, which the attacker does not have access to.
Second, after the instruction is finished, Sancus' access rules (\cref{flt:access_rights}) will deny any access to the text section from outside the module.
Therefore, only API-level attacks would enable an attacker to read (parts of) the text section of modules that use confidential loading.

Finally, \emph{hardware breach confinement} follows from the fact that we use independent master keys on all nodes (\cref{sec:design:keys}).
