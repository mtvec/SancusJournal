
\section{Application Scenarios}
\label{sec:apps}

We have investigated application scenarios for Sancus in the context of
trust assessment for \ac{IoT} devices and sensor
nodes~\cite{muehlber:sancus-ta}, and for building a secure smart
electricity meter for the emerging smart electricity
grid~\cite{muehlber:smart_meter}. Both applications yield promising
results, in particular with respect to system performance and the size of
the software \ac{TCB}. In this section we briefly present our findings from
these two scenarios, and outline challenges and future applications in
domains such as embedded real-time control systems.

% ----------------------------------------------------------------------------
\subsection{Trust Assessment Modules for the \acl{IoT}}

Devices in the \ac{IoT} or in \acp{WSN} are typically equipped with
inexpensive low-performance microcontrollers. Yet, these devices are
interconnected and thereby exposed to physical as well as virtual
attacks~\cite{roman:securing_iot}. Even when not considering malicious
interference, the device's autonomous mode of operation,
exposure to harsh environmental conditions and resource scarceness,
make these systems prone to malfunction and the effects of software aging.

The problem of trustworthiness and trust management of low-power
low-performance computing nodes has been discussed in previous research, in
particular in the context of
\acp{WSN}~\cite{fernandez-gago:wsn_trust_management_survey,granjal:wsn_internet_survey,lopez:trust_management_wsn}.
Most techniques proposed focus on observing the communication behavior and
on validating the plausibility of sensor readings obtained from nodes, so
as to assess the trustworthiness of these nodes. This approach is certainly
suitable to detect the systematic failure or misbehavior of single nodes.
However, detection is not immediate and a malfunctioning node may output
corrupted data that is not labelled as such, before the network will begin
to distrust the node: the quality of readings from a sensor may degrade
gradually, software failures may lead to non-deterministic behavior or a
node may be captured by an attacker, exposing benign and malicious behavior
alternately.

In \cite{muehlber:sancus-ta} we show that the above shortcoming can be mitigated
by employing Sancus in an approach to securely obtain
measurements with respect to the integrity of the software that runs on a
minimalist computing node autonomously or on demand. We use these
measurements as an indication of the trustworthiness of that node.
Sancus allows us to integrate trust assessment modules into a
largely unmodified and untrusted embedded \ac{OS} without using techniques
such as virtualization and hypervisors, which would incur unacceptable
performance overheads for many embedded applications. With Sancus'
remote attestation functionality, our trust assessment modules can be
deployed dynamically, limiting memory consumption and restricting attacker
adaptation. The module may then inspect the \ac{OS} or application code and
securely report trust metrics to an external trust management system.

We describe and evaluate a prototype implementation of our approach that
integrates Sancus-protected trust assessment modules with the
Contiki \ac{OS}~\cite{dunkels:contiki}, measuring properties such as code
integrity, the content of \ac{OS}- and application data structures, the
availability of system resources or the occurrence of system events.
As
an example, we have implemented a trust assessment module that monitors the
process list maintained by Contiki's scheduler, and checks code integrity
of the processes present in the above list. To test the effectiveness of
our trust assessment module, our scenario integrates a number of trivial
application processes and an attacker process that aims to perform
alterations to \ac{OS} data and application code. Expectedly, all changes
performed by the attacker are detected and reported with the subsequent
invocation of the trust assessment module. Our results demonstrate that,
using Sancus, comprehensive inspection mechanisms can be implemented
efficiently, incurring runtime overheads that should be acceptable in many
deployment scenarios with stringent requirements with respect to safety and
security. Indeed, we believe that our approach enables many
state-of-the-art inspection mechanisms and countermeasures against
attacks~\cite{llsbe} to be adapted for \ac{IoT} nodes and in the domain of \acp{WSN}.


% ----------------------------------------------------------------------------
\subsection{High Assurance Smart Metering}

With the rise of the smart electricity grid and the extended use of
renewable energy resources, there is need for appliances and metering
equipment to become smart. Smart appliances can use electricity efficiently
when it is inexpensive due to, e.g., local production or the behavior of
other users of the grid. Smart electricity meters must therefore timely
communicate measurements of local energy consumption and production to grid
operators. Being part of the critical infrastructure of our society,
security of the smart grid must be guaranteed, not only at the level of the
grid operator but also at the level of individual premises.

In~\cite{muehlber:smart_meter} we implement a simplified scenario for smart
meter deployment and evaluate security aspects of this scenario.
Our implementation is loosely
based on the British \enquote{Smart Metering Implementation
Programme}~\cite{decc:smart_meter_spec} but simplifies
communication protocols and adopts architectural changes suggested
in~\cite{cleemput:ha_smart_metering}, relying on Sancus to implement
security features.  Importantly, the goal of
this case study is \emph{not} to accurately
implement~\cite{decc:smart_meter_spec} but to provide a security-focused
reference implementation
that illustrates the use of Sancus to
achieve a notion of \emph{high assurance smart metering} by means of
logical component isolation, mutual authentication, and by minimizing the
software \ac{TCB}.

Our scenario contains software components that implement a smart
electricity meter to be installed at a client's premises, and a Load Switch
that can enable or disable power supply to the premises. We further
implement components to represent the grid operator's Central System and an
In-Home Display. The smart meter and the Load Switch communicate with the
Central System via a \ac{WAN} Interface.  In our case, the \ac{WAN}
Interface supports periodic access to the smart meter's operational data,
as well as control of the Load Switch.  The smart meter and the In-Home
Display communicate via the \ac{HAN} Interface. Only consumption data is
periodically sent from the smart meter to the In-Home Display via this
interface.  All components are meant to be deployed as protected software
modules on a Sancus-like infrastructure that facilitates software component
isolation and authenticated and secure communication between these modules.
Relying on these security primitives, our approach and prototype guarantee
that all outputs of the software system can be explained by the system's
source code and the actual physical input events. We further guarantee
integrity and confidentiality of messages while relying on a very small
software \ac{TCB} at runtime --  less than 300~LOC, excluding drivers.  For
scenarios that involve critical infrastructure, such as the smart grid, we
believe that our approach has the strategic advantage of enabling formal
verification and security certification of small, isolated software
components while maintaining the strong security guarantees of the
distributed system that is formed by the interaction of these components.

% ----------------------------------------------------------------------------
\subsection{Challenges and Future Directions}

In the past few years we have seen a number of attack scenarios in which
safety-critical infrastructure was successfully compromised and abused at
the site of the end user. Amongst the most prominent cases are certainly
remote attacks against insulin pumps~\cite{radcliffe:fun_and_insulin},
general hospital equipment~\cite{erven:what_the_doctor_ordered}, and
against automotive vehicles~\cite{miller:remote_car_exploit}. In all these
cases the initial subject of an attack was software. In particular,
software that would interact with critical components of the appliance but
that was not considered safety-critical itself.  We believe that these
attack scenarios would become infeasible or at least very hard to implement
if critical software components would be
integrity protected, resistant against certain code misuse attacks, not
vulnerable to low-level attacks that exploit implementation details, and
use authenticated encryption to securely communicate with each other.
Sancus provides a trusted computing
infrastructure to securely implement the above scenario,
excluding many attack vectors by design, in particular when combined with
fully abstract compilation~\cite{fully-abstract-pma}.

In future application scenarios for Sancus we will investigate authentic
execution for distributed event-driven applications that execute on a
heterogeneous shared infrastructure and require a small \ac{TCB}. Similar
to the above example of a high assurance smart meter, these applications
are characterized by consisting of multiple components that execute on
different computing nodes and for which program flow is determined by
events such as the occurrence of sensor readings.

As an example, consider an automotive anti-lock braking system (ABS) with
its sensors (pedal and rotation) and actuators (brake hydraulics). For
these systems, we strive for a notion of authentic execution that entails,
roughly speaking, the following: if the application produces a physical
output event (e.g., engaging the brakes), then there must have happened a
sequence of physical input events (sensor readings) such that this
sequence, when processed by the application (as specified by the
application's source code), produces that output event.  This provide
strong integrity guarantees, ruling out both spoofed events as well as
tampering with the execution of the program. The problem of trustworthiness
of automotive control systems has been addressed in recent standards and
research (e.g.,~\cite{autosar42,nurnberger:vatican}), albeit without
consideration of software security and software integrity that our approach
can provide.  Ongoing research further investigates the reliability of
automotive sensor readings, which can be subject to active spoofing attacks
(cf.~\cite{shoukry:spoofing_attack,shin:sampling_race}). Generally, these
vulnerabilities have to be addressed at the physical layer and are out of
scope for Sancus.

Handling input and output events requires the ability to implement secure I/O drivers.
\Cref{sec:design:example} already described a simple way to implement such a driver: by mapping its data section over the memory-mapped I/O region of a device, the driver gains exclusive access over this device.
However, future work will have to address open issues such as sharing devices between multiple modules and securely delivering interrupts to drivers.

Yet, implementing distributed control systems based on Sancus or similar
technology is difficult beyond achieving security guarantees. In ongoing
work we address challenges with respect to efficiently implementing
resource sharing between Sancus modules, and the problem of providing
availability and real-time guarantees.

\subsubsection{Implementing Resource Sharing and Access Control}
%
Traditionally,
software for small microcontrollers relies heavily on implementing
communication between software components via shared memory, and sharing
I/O devices between different components. Of course, this has security
implications such that software components must mutually trust each other.
Isolating different software components as protected modules, however,
limits their ability to securely share system resources, asking for new
development paradigms and potentially imposing performance overheads.
In~\cite{vanbulck:sancus_ac}, we describe and evaluate an approach to
implement and securely enforce application-grained access control policies
for \ac{IoT} nodes. Our access control mechanism can manage access to
various system resources such as file systems, I/O devices, or specific
devices attached to an external communication bus. While incurring low
overheads, our mechanism guarantees at runtime that only authenticated
software modules gain access to resources as specified in the policy; the
internal state of the access control implementation is protected and cannot
be tampered with. We evaluate a prototypic implementation of our access
control mechanism in two application scenarios that facilitate secure data
sharing between software modules through
\begin{paraenum}
  \item a shared memory implementation and
  \item peripheral flash memory and the Coffee~\cite{tsiftes2009enabling} file system.  
\end{paraenum}
Our evaluation shows that module isolation and access control impose relatively
low overheads that should be acceptable in deployment scenarios with
stringent safety and security requirements.

\subsubsection{Availability and Real-Time Guarantees for Sancus}
%
The security guarantees offered by current protected module architectures
are limited to confidentiality and integrity guarantees -- they do not
extend to \emph{availability}.  A  buggy or malicious application can still
harm the availability of the platform by overwriting crucial \ac{OS} data
structures, or by monopolizing a shared system resource such as CPU time.
In~\cite{vanbulck:rt_pma}, we report on our work-in-progress towards lifting
this limitation.  We argue that, in addition to isolating software,
hardware-level protection mechanisms can be extended to also preserve
availability (possibly even real-time) guarantees on a partially
compromised embedded system. The objective of this line of work is to
address some of the availability challenges induced by hardware-level
protected module architectures, while keeping the \ac{TCB} small.
We outline a hardware mechanism that makes Sancus' protected
modules fully interruptible and reentrant.  We show how our mechanism preserves
deadlines for external events, and facilitates reasoning about real-time
guarantees by ensuring a deterministic interrupt latency at all times. We sketch
a multitasking model that introduces protection domains \emph{within} a
conventional control flow thread and show how logical threads can be
managed by an unprivileged scheduler.

