#!/usr/bin/env python3

import csv
import collections

import matplotlib.pyplot as plt
from cycler import cycler

import numpy as np


class PlotData:
    def __init__(self, x_name, y_name):
        self.x_name = x_name
        self.y_name = y_name
        self.x = []
        self.y = []

    def add(self, x, y):
        self.x.append(x)
        self.y.append(y)

    def __repr__(self):
        return 'x: {}, y: {}'.format(self.x, self.y)


def gen_plot_data(file_name):
    with open(file_name, 'r', newline='') as f:
        reader = csv.reader(f)
        x_name, *y_names = next(reader)
        results = [PlotData(x_name, y_name) for y_name in y_names]

        for line in reader:
            assert len(line) == len(y_names) + 1

            x = float(line[0])

            for i in range(1, len(line)):
                y_str = line[i].strip()

                if len(y_str) != 0:
                    results[i - 1].add(x, float(y_str))

        return results


def gen_max_speed_plot():
    for data in gen_plot_data('max_speed.csv'):
        plt.plot(data.x, data.y, label='{} bit'.format(data.y_name))

    plt.xlabel(r'$N_{SM}$')
    plt.ylabel('Maximum CPU frequency (MHz)')
    plt.ylim(ymax=41)
    plt.legend(loc='lower left')
    plt.minorticks_on()
    plt.grid()
    plt.savefig('max_speed.pgf')
    plt.clf()


def gen_micro_plot():
    for data in gen_plot_data('micro.csv'):
        plt.plot(data.x, data.y, label='{} bit'.format(data.y_name))

    plt.xlabel(r'Input size (B)')
    plt.ylabel('Execution time (cycles)')
    plt.legend(loc='upper left')
    plt.minorticks_on()
    plt.grid()
    plt.savefig('micro.pgf')
    plt.clf()


def gen_macro_plot():
    base_1st = 26997
    base_nth = 3644
    base_ref = 163

    def f_1st(work):
        return base_1st + work

    def f_nth(work):
        return base_nth + work

    def f_ref(work):
        return base_ref + work

    plt.figure(figsize=(10, 5))
    x = np.arange(5000)
    plt.plot(x, f_1st(x) / f_ref(x), '-', label='First run')
    plt.plot(x, f_nth(x) / f_ref(x), '-', label='Subsequent runs')

    plt.xlabel('Number of cycles of useful work')
    plt.ylabel('Relative overhead')
    plt.legend()
    plt.minorticks_on()
    plt.savefig('macro.pgf')
    plt.clf()


def gen_area_plot():
    fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(10, 5))

    for ax in (ax0, ax1):
        ax.minorticks_on()
        ax.grid()

    for data in gen_plot_data('area_regs.csv'):
        ax0.plot(data.x, data.y, label='{} bit'.format(data.y_name))

    ax0.set_xlabel(r'$N_{SM}$')
    ax0.set_ylabel('Number of slice registers')
    ax0.legend(loc='upper left')

    for data in gen_plot_data('area_luts.csv'):
        ax1.plot(data.x, data.y, label='{} bit'.format(data.y_name))

    ax1.set_xlabel(r'$N_{SM}$')
    ax1.set_ylabel('Number of slice LUTs')

    fig.tight_layout()
    plt.savefig('area.pgf')
    plt.clf()

def gen_asic_area_plot():
    fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(10, 5))

    for ax in (ax0, ax1):
        ax.minorticks_on()
        ax.grid()

    for data in gen_plot_data('area_asic_umc130.csv'):
        ys = [y / 1000 for y in data.y]
        ax0.plot(data.x, ys, label='{} bit'.format(data.y_name))

    ax0.set_title('UMC 130nm')
    ax0.set_xlabel(r'$N_{SM}$')
    ax0.set_ylabel('kGE')

    for data in gen_plot_data('area_asic_nangate15.csv'):
        ys = [y / 1000 for y in data.y]
        ax1.plot(data.x, ys, label='{} bit'.format(data.y_name))

    ax1.set_title('NanGate 15nm')
    ax1.set_xlabel(r'$N_{SM}$')
    ax1.set_ylabel('kGE')
    ax1.legend(loc='upper left')

    fig.tight_layout()
    plt.savefig('area_asic.pgf')
    plt.clf()


plt.rc('lines', linewidth=2)
plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y']) +
                           cycler('marker', ['D', '^', 'o', 's'])))

#gen_max_speed_plot()
#gen_micro_plot()
#gen_macro_plot()
#gen_area_plot()
gen_asic_area_plot()
