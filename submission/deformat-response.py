#!/usr/bin/env python3

import sys

for line in sys.stdin:
    if line.startswith('>'):
        print(' ' * (len(line) - 1))
    else:
        print(line, end='')

